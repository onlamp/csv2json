'use strict';

const NODE_ENV = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");

function isDevelopment() {
  return  NODE_ENV == 'development';
}

function isProduction() {
  return  NODE_ENV == 'production';
}

module.exports = {
  context: __dirname + '/assets',
  entry: {
    app: ['bootstrap-loader/extractStyles', './js/app.coffee']
  },

  output: {
    path:     __dirname + '/tmp/assets',
    filename: "/js/[name].js",
    library: "[name]",
    publicPath: '../'
  },

  watch: isDevelopment(),

  watchOptions: {
    aggregateTimeout: 100
  },

  //devtool: isDevelopment() ? "cheap-inline-module-source-map" : null,
  devtool: isDevelopment() ? "source-map" : null,

  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.js']
  },

  module: {
    loaders: [
      // coffee
      { test: /\.coffee$/, loader: "coffee" },

      { test: /\.scss$/, loaders: [ 'style', 'css', 'sass' ] },
      //Шрифты
      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url?limit=10000&name=/fonts/[name].[ext]"
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        loader: 'file?name=/fonts/[name].[ext]'
      },
      // Картинки
      {
        test: /\.(png|gif|jpg|jpeg)(\?[\s\S]+)?$/,
        loader: "url?limit=10000&name=[path][name].[ext]"
      },
      // Bootstrap 3
      { test: /bootstrap-sass\/assets\/javascripts\//, loader: 'imports?jQuery=jquery' },
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new ExtractTextPlugin("/css/app.css" ),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
      NODE_ENV: JSON.stringify(NODE_ENV)
    })
  ],

};

/*
if (isProduction()) {
  module.exports.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        // don't show unreachable variables etc
        warnings:     false,
        drop_console: true,
        unsafe:       false
      }
    })
  );
}
  */