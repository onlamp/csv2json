angular = require 'angular'
ui_bootstrap = require 'angular-ui-bootstrap'

app = angular.module('csv2json', [ui_bootstrap])
app.controller 'ConvertController', ($scope)->
  $scope.csv = {
    text: ''
    checked_separators: [true,true]
    custom_separator: ''
    name: 0
    separators: []
  }
  $scope.csv.type=0
  $scope.csv.view=0
  $scope.json = ''
  $scope.csv_separators=[[';',';'],[',',','],['Tab','\t']]
  $scope.msgs=[]


  $scope.$watch 'csv', ()->
      $scope.csv.separators=get_separators()
      if $scope.csv.separators!=[]
        arr=csv_to_arr($scope.csv)
        arr_to_json(arr)
    , true

  $scope.showContent = ($fileContent)->
    $scope.csv.text = $fileContent

  get_separators=()->
    separators=[]
    for v,k in $scope.csv.checked_separators
      separators.push $scope.csv_separators[k][1] if v==true
    if $scope.csv.custom_separator and $scope.csv.custom_separator!=''
      separators.push $scope.csv.custom_separator
    return separators

  csv_to_arr=()->
    arr=null
    # отлуп если нет разделителя
    return 'Separator not selected' if $scope.csv.separators.length==0
    # Делаем регулярку по которой разбиваем на части
    re=new RegExp($scope.csv.separators.join('|'))
    lines=[]
    lines=$scope.csv.text.split(/\r?\n+/) if $scope.csv.text!=''
    return lines if lines.length==0
    # Получаем ключи хеша из первой строки
    if $scope.csv.name>0 or $scope.csv.type>0
      keys=lines[0].split(re)
      lines.splice(0,1)
      # отлуп если только одна строка
      return 'one string' if lines.length==0
    # разбираем оставшиеся строки в зависимости от выбранного типа
    arr=[]
    switch $scope.csv.type
      when 0
        for line in lines
          arr.push line.split(re)
      when 1
        for line in lines
          obj_item={}
          for i,v of line.split(re)
            obj_item["#{keys[i]}"]=v
          arr.push obj_item
      when 2
        for line in lines
          obj_item={}
          arr_line=line.split(re)
          id=arr_line[0]
          arr_line.splice(0,1)
          obj_item["#{id}"]={}
          for i,v of arr_line
            obj_item["#{id}"]["#{keys[parseInt(i)+1]}"]=v
          arr.push obj_item

    return arr

  arr_to_json=(arr)->
    $scope.json=''
    return if arr.length==0
    switch $scope.csv.view
      when 0
        $scope.json=JSON.stringify arr
      when 1
        $scope.json=JSON.stringify arr, null, '  '
      when 2
        $scope.json="function(#{JSON.stringify(arr)});"

app.directive 'onReadFile', ($parse)->
  return {
    restrict: 'A',
    scope: false,
    link: (scope, element, attrs)->
      fn = $parse(attrs.onReadFile)
      element.on 'change', (onChangeEvent)->
        reader = new FileReader()
        reader.onload = (onLoadEvent)->
          scope.$apply ()->
            fn scope, {$fileContent: onLoadEvent.target.result}
        reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0])
  }