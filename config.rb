###
# Page options, layouts, aliases and proxies
###

# Integrate Dotenv
activate :dotenv

# Activate gzip compression
activate :gzip

activate :deploy do |deploy|
  deploy.deploy_method = :git
  deploy.remote = 'https://github.com/avaslev/csv2json.git'
end

# Set html template engine
set :slim, { ugly: true }

# Site config
config[:host] = ""
config[:path] = ""
config[:site_url] = config[:host]+config[:path]
config[:title] = "csv2json"

# Asset paths
config[:js_dir] = "#{config[:path]}/js"
config[:css_dir] = "#{config[:path]}/css"
config[:images_dir] = "#{config[:path]}/img"
config[:fonts_dir] = "#{config[:path]}/fonts"

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

# General configuration

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload, host: 'localhost'

  activate :external_pipeline,
           name: :webpack,
           command: build? ? './node_modules/webpack/bin/webpack.js --bail' : './node_modules/webpack/bin/webpack.js --watch -d',
           source: './tmp/assets',
           latency: 1
end

###
# Helpers
###

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

# Build-specific configuration
configure :build do
  # Site config
  config[:host] = "http://avaslev.github.io"
  config[:path] = "/csv2json"
  config[:site_url] = config[:host]+config[:path]
  config[:title] = "csv2json"

  # Asset paths
  config[:js_dir] = "#{config[:path]}/js"
  config[:css_dir] = "#{config[:path]}/css"
  config[:images_dir] = "#{config[:path]}/img"
  config[:fonts_dir] = "#{config[:path]}/fonts"

  activate :external_pipeline,
           name: :webpack,
           command: 'NODE_ENV=production ./node_modules/webpack/bin/webpack.js --bail',
           source: './tmp/assets',
           latency: 1

  # Minify CSS on build
  activate :minify_css

  # Minify Javascript on build
  # activate :minify_javascript
end
